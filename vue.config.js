module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === 'production'
    ? `${process.env.CI_PROJECT_PATH.replace(process.env.CI_PROJECT_ROOT_NAMESPACE, '')}/`
    : '/',
};
